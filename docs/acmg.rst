Secondary findings
^^^^^^^^^^^^^^^^^^

.. caution::
   The American College of Medical Genetics and Genomics (ACMG) discourages the use of genes in ACMG SF v2.0 for purposes other than reporting incidental findings after *clinical sequencing*.
   Therefore the use of variants in this section outside of *clinical sequencing* is not recommended.
   (based on `ACMG statement on the use of ACMG secondary findings recommendations for general population screening <https://www.nature.com/articles/s41436-019-0502-5>`_)


The ACMG has published `recommendations <https://www.nature.com/articles/gim2016190>`_ for reporting incidental findings in the exons of certain genes (`ACMG SF v2.0 <https://www.ncbi.nlm.nih.gov/clinvar/docs/acmg/>`_).
The recommendation specifies, for clinical sequencing of each phenotype, genes and variants to report as secondary findings.

CGAR list variants on any of genes listed in ACMG SF v2.0, along with which phenotype the gene was recommended for (``ACMG phenotype`` column for phenotype and ``MIM ID`` for MIM identifyer of the phenotype).
Given the lack of the knowledge on phenotype or the purpose of sequencing, CGAR will always list variants on any of genes listed in ACMG SF v2.0.
However, users are recommended to use this information only in a way compliant to ACMG recommendation and statement.

Additionally, CGAR shows phenotype and variant category from HGMD\ |reg| (requires license) (columns ``HGMD phenotype`` and ``HGMD class variant``).

.. |reg| unicode:: U+000AE .. REGISTERED SIGN
