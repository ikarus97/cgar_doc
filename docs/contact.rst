Contact
-------

General inquiries may be addressed to `Sek-Won Kong <SekWon.Kong@childrens.harvard.edu>`_.

Specific questions regarding the use of CGAR and local installation, please send an email to `gNOME staff <gnome.pipeline@gmail.com>`_.
