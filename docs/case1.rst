An Example Use Case
^^^^^^^^^^^^^^^^^^^

Method - filter variants by allele frequency
""""""""""""""""""""""""""""""""""""""""""""

Let's use an artificial sample for Miller syndrome to illustrate steps to identify variants potentially associated with phenotype.

First, select the sample **Miller** from the list of samples, then click ``Generate report`` to start analysis.

.. image:: /_static/select_sample.png
   :scale: 70 %
   :align: center


By default, the ``ClinVar`` tab will show 6 variants as shown below.

.. image:: /_static/report_example.png

However, given the fact that the incidence of Miller syndrome is one in a million newborns (`ref <https://ghr.nlm.nih.gov/condition/miller-syndrome>`_), it could be useful to restrict our search to vary rare variants (allele frequency less than 0.5%), then click ``Generate report``.
(based on `ACMG AMP guideline for the interpretation of sequence variants <https://doi.org/10.1038/gim.2015.30>`_, Table 4: Criteria for benign variants, BS1 - allele frequency is greater than expected for disorder.)

.. image:: /_static/miller_2.png

This will leave us with only 3 variants: 2 missense variants on *DHODH* gene and one missense variant on *PRODH* gene.
Of these, the 2 variants on *DHODH* gene were the disease-assoiciated variants that were added to create artificial samples.


Alternative method - search variants by genes associated with phenotype
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Alternatively, we could search for genes associated with the phenotype and identify variants on those genes, within ``Phenotype associated`` tab.
Since we know that the variants are from sample with Miller syndrome, type in **Miller** into the ``Phenotype`` text box.
Also, to limit our search for the most relevant genes, we select the top 1% of the genes (select **1** from the dropdown value beneath the ``Phenotype`` text box).
We also choose to see any variants that are of high or moderate impact: select both **High impact variants** and **Moderate impact variants** under the ``Calculated variant consequences``, then click ``Generate report``.

.. image:: /_static/miller_3.png

Again, we could see the 2 missense variants on *DHODH* gene (the same variants we found in the previous method), and another missense variant on *CYP21A2* gene.


This example illustrates how CGAR can identify known disease-associated variants or rare variants in the genes associated with phenotype.
