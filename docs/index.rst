.. CGAR documentation master file, created by
   sphinx-quickstart on Tue Jul 23 19:32:37 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CGAR's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:
   :glob:

   intro
   upload_sample
   generate_report
   report_sections
   installation
   contact
