.. _setup:

Running CGAR locally
----------------------

Requirements
^^^^^^^^^^^^

* `Docker <https://www.docker.com/>`_: any versions of Docker that support docker compose file version 2 and ``docker-compose``.
* CGAR distribution: contains Docker image construction files and source code. Plase contact `us <gnome.pipeline@gmail.com>`_ for getting the file.
* Free disk space of 700MB


Installation
^^^^^^^^^^^^

First, unpack CGAR distribution file (``cgar-dist.tar.gz``):

.. code-block:: bash

   $ cd /path/to/distribution/file
   $ tar -xzvf [filename]
   $ cd cgar-dist

Next, build Docker images for CGAR (two images for CGAR database and web application, respectively):

.. code-block:: bash

   # build all images with docker-compose
   $ cd /path/to/docker-compose.yaml
   $ docker-compose build
   # see if images appear in the list of available Docker images
   $ docker images

When successful, Docker images ``cgar-dist_cgar_web`` and ``cgar-dist_cgar_db`` will be created.

Then, adjust application settings for local environment in ``docker-compose.yml``:

.. code-block:: yaml

   version: '2'
   services:
     cgar_db:
       build: ./cgar_db
       volumes:
         - ./cgar_web/db:/docker-entrypoint-initdb.d
         - /path/to/save/mysqldata:/var/lib/mysql ## [ UPDATE LOCAL PATH ]
         - /path/to/save/dbkey:/var/lib/mysql-keyring:rw ## [ UPDATE LOCAL PATH ]
       environment:
         - MYSQL_ROOT_PASSWORD=*your_password* ## [ UPDATE THIS ]
         ...
         - MYSQL_PASSWORD=*temp* ## [ UPDATE THIS ]
       command: --early-plugin-load=keyring_file.so
     cgar_web:
       build: ./cgar_web
       volumes:
         - /path/to/vep/cache:/cache:rw ## [ UPDATE LOCAL PATH ]
       ports:
         - "CGAR_PORT:80" ## [ CHANGE PORT NUMBER ]
       expose:
         - "CGAR_PORT" ## [ CHANGE PORT NUMBER ]
       depends_on:
         - cgar_db
       environment:
         ...
         - CGAR_DB_PASSWORD=*temp*   ## [ UPDATE THIS ]
         - VEP_CACHE_DIR=/cache
         - LOW_MEM=1        ## [ UPDATE THIS ]
         - VEP_CACHE_VER=92  ## [ UPDATE THIS ]
         - COSMIC_USER=some.email@institute.edu ## [ UPDATE THIS ]
         - COSMIC_PW=password ## [ UPDATE THIS ]
   ...

Settings to be customized in ``cgar_db`` section:

* ``/path/to/save/mysqldata``: persistent storage for mySQL database tables that will be used in CGAR. Should point to local folder where Docker container can read and write, and have enough disk space for database files (grows over time). If the folder does not exist already, ``dockder-compose`` will create it for you.
* ``/path/to/save/dbkey``: persistent location for mySQL encryption key. Should point to **an existing** local folder where Docker container can read and **write**.
* ``MYSQL_ROOT_PASSWORD``: this is administrator password for the entire mySQL database in CGAR. Not directly used by CGAR.
* ``MYSQL_PASSWORD``: mySQL password for CGAR application only. Should also be the same as ``CGAR_DB_PASSWORD`` under ``cgar_web`` section.

Settings to be customized in ``cgar_web`` section:

* ``/path/to/vep/cache``: persistent location to store annotation files for VEP. Should have enough space for VEP cache files (~15GB).
* ``CGAR_PORT``: the port number for CGAR web application.
* ``CGAR_DB_PASSWORD``: mySQL password for CGAR application only. Should also be the same as ``MYSQL_PASSWORD`` under ``cgar_db`` section.
* ``LOW_MEM``: keep the line if the installed memory (or memory available by Docker) < 40GB. Otherwise delete or comment out this line.
* ``VEP_CACHE_VER``: the version of VEP annotation to be used in CGAR. Change this to use the latest VEP version.
* ``COSMIC_USER`` and ``COSMIC_PW``: for cancer-associated genes and variants, CGAR uses data files from `COSMIC <https://cancer.sanger.ac.uk>`_ (requires COSMIC account). CGAR will use this COSMIC account information to get data fiiles (downloaded only once during the first time of running).
   
Lastly, start and initalize CGAR application.

.. code-block:: bash

   # start CGAR container for the first time
   $ cd /path/to/docker-compose.yml
   $ docker-compose up -d
   # check if containers are up and running
   $ docker container ls
   ...
   # initialize VEP cache and other internal resources
   # (these will take a lot of time...)
   # 1. download and build VEP annotations
   $ docker -it exec (cgar_web image name) /bin/bash /cgar/build_cache.sh
   # 2. prepare ClinVar annotations
   $ docker -it exec (cgar_web image name) /bin/bash /cgar/prep_ref_table_clinvar.py
   # 3. prepare COSMIC annotations
   $ docker -it exec (cgar_web image name) /bin/bash /cgar/prep_ref_table_census.py
   $ docker -it exec (cgar_web image name) /bin/bash /cgar/prep_ref_table_cosmic.py
   # 4. prepare denovoDB annotations
   $ docker -it exec (cgar_web image name) /bin/bash /cgar/prep_ref_table_denovodb.py
   

.. note::
   The first time of running CGAR images could take a long time due to the amount of time to build internal database.

Afterwards, if necessary, stop and start CGAR containers as follows.

.. code-block:: bash

  $ cd /path/to/docker-compose.yml
  $ docker-compose stop
  $ docker-compose start


Access local CGAR
^^^^^^^^^^^^^^^^^

Assuming the port number ``8081`` was used for ``CGAR_PORT``, open url ``http://localhost:8081``.

Source Code
^^^^^^^^^^^
* Interested users can find the source code for CGAR is available from `here <https://bitbucket.com/gnome_pipeline/cgar_pub>`_.
